version: '3.7'
services:
  minio:
    image: minio/minio:2024.6.13-debian-12-r2
    command: server --console-address ":9001" /data/
    ports:
      - "${S3_API_PORT}:9000"
      - "${S3_WEB_UI_PORT}:9001"
    environment:
      MINIO_ROOT_USER: ${MINIO_ROOT_USER}
      MINIO_ROOT_PASSWORD: ${MINIO_ROOT_PASSWORD}
      MINIO_ACCESS_KEY: ${S3_ACCESS_KEY}
      MINIO_SECRET_KEY: ${S3_SECRET_KEY}
    volumes:
      - minio-storage:/data
    healthcheck:
      test: ["CMD", "curl", "-f", "http://localhost:9000/minio/health/live"]
      interval: 30s
      timeout: 20s
      retries: 3

  postgres:
    image: postgres:16.3
    ports:
      - '5432:5432'
    environment:
      POSTGRES_DB: ${POSTGRES_DB}
      POSTGRES_USER: ${POSTGRES_USER}
      POSTGRES_PASSWORD: ${POSTGRES_PASSWORD}
    volumes:
      - postgres-data:/var/lib/postgresql/data

  mlflow:
    build:
      context: ./mlflow
      dockerfile: Dockerfile
    ports:
      - "5000:5000"
    environment:
      AWS_ACCESS_KEY_ID: ${S3_ACCESS_KEY}
      AWS_SECRET_ACCESS_KEY: ${S3_SECRET_KEY}
      MLFLOW_S3_ENDPOINT_URL: ${SERVER_SCHEMA}://${SERVER_HOST}:${S3_API_PORT}
    entrypoint: >
      mlflow server 
      --backend-store-uri postgresql+psycopg2://${POSTGRES_USER}:${POSTGRES_PASSWORD}@postgres/${POSTGRES_DB} 
      --default-artifact-root s3://${MLFLOW_BUCKET}/ 
      -h 0.0.0.0
    depends_on:
      - postgres

  prometheus:
    image: bitnami/prometheus:2.53.0-debian-12-r0
    container_name: prometheus
    ports:
      - 9090:9090
    volumes:
     - ./config/prometheus.yml:/etc/prometheus/prometheus.yml
     - prometheus_data:/opt/prometheus/data
    command:
      - '--config.file=/etc/prometheus/prometheus.yml'

  grafana:
    image: bitnami/grafana:10.4.4-debian-12-r0
    container_name: grafana
    ports:
     - 3000:3000
    volumes:
     - ./config/datasource.yml:/etc/grafana/provisioning/datasource.yml
     - grafana_data:/var/lib/grafana

  loki:
    image: grafana/loki:latest
    ports:
      - "3100:3100"


volumes:
  minio-storage:
  prometheus_data:
  grafana_data:
  postgres-data:
